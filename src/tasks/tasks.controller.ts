import { Body, Controller, Delete, Get, Param, Patch, Post, Put } from '@nestjs/common';
import { Task } from './task.model';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {

    constructor(
        private tasksService: TasksService
    ){}
    
    @Post()
    async createTask
    (@Body() body:Task):Promise<Task[]>{
    const tasks = await this.tasksService.crateTask(body);
    return tasks;
    }


    @Get()
    async getTasks(): Promise<Task[]>{
     const tasks   =await this.tasksService.getTasks();
     return tasks;
    }

    @Get(':id')
    async getOneTask(@Param('id') id:string):Promise<Task>{
     const tasks   =await this.tasksService.getOneTask(id);
     return tasks;    
    }

    @Delete(':id')
    async deleteTask(
        @Param('id') id:string
    ):Promise<string>{
     const tasks  = await this.tasksService.deleteTask(id)
     return tasks;
    }

    @Put(':id')
    async updateTask(
        @Param('id') id:string,
        @Body() body: Task
    ):Promise<Task> {
     const tasks   = await this.tasksService.updateTask(id,body);
     return tasks;
    }
    
   
}
