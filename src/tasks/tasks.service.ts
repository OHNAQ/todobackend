import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { Task } from './task.model';

@Injectable()
export class TasksService {
    tasks: Task[] =[
        {id:'1654843274797' ,Daily:'Play football and homework',date:new Date()},
        {id:'1655834595132' ,Daily:'Pracetice table tennis',date:new Date()},
        {id:'1653512567212' ,Daily:'Play VDO Games all night',date:new Date()}
    ];
    
    //Create Task
  async  crateTask(task:Task):Promise<Task[]>{
        if(task.Daily === ''){
        throw  new BadRequestException();
    }
    const taskToCreate:Task = await {
        id: new Date().getTime().toString(),
        date:new Date(),
        Daily: task.Daily
    }
    this.tasks.push(taskToCreate);

    return [...this.tasks];
    }


    //Get All Tasks
  async  getTasks(): Promise<Task[]>{
        return [...this.tasks];
    }

    //Get One Task
  async  getOneTask(id: string): Promise<Task>{
        const task = await this.findTask(id);
        if(!task){
            throw new NotFoundException();
        }
        return{...task};
    }

    //Delete One Task
   async deleteTask(id:string):Promise<string>{
        const task =await this.findTask(id);

        if (!task){
            throw new NotFoundException();
        }
        const index =await this.tasks.indexOf(task);
        this.tasks.splice(index,1);
        
        return  id;
    } 
        private async findTask(id:string):Promise <Task> {
            const task =await this.tasks.find(t=> t.id=== id);
            return task
        }
    //Updata task
  async  updateTask(id:string,task:Task):Promise<Task>{
      if (task.Daily===''){
          throw new BadRequestException();
      }
      const index =await this.tasks.findIndex(t=>t.id === id)
      this.tasks[index] = {...this.tasks[index],...task};
      return {...this.tasks[index]}
    }
}
